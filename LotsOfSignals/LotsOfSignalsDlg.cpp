﻿
// LotsOfSignalsDlg.cpp: файл реализации
//



#define _USE_MATH_DEFINES
#include <math.h>
#include "pch.h"
#include "framework.h"
#include "LotsOfSignals.h"
#include "LotsOfSignalsDlg.h"
#include "afxdialogex.h"
#include <cmath>
#include <vector>
#include <complex>
#include "Generation.h"
#include "Methods.h"
#include <fstream>
//#include <gdiplus.h>
#include <iomanip>
#include <time.h>
//#include "Signal.h"
//#include "Generation.h"
#include <GL/gl.h>
#include <GL/glu.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


using namespace std;
// Диалоговое окно CLotsOfSignalsDlg
#define KOORD(x,y) (xp*((x)-xmin)),(yp*((y)-ymax)) 
#define KOORDGET(x,y) (xpget*((x)-xminget)),(ypget*((y)-ymaxget)) 


int timer = 0;

DWORD WINAPI opglstream(PVOID n)
{
	DWORD c;
	CLotsOfSignalsDlg* g = ((CLotsOfSignalsDlg*)n);
		//g->DrawGL(g->outsignal2D);
	return (0);

}


CLotsOfSignalsDlg::CLotsOfSignalsDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_LOTSOFSIGNALS_DIALOG, pParent)
	, A1(3)
	, A2(2)
	, A3(4)
	, F1(0.05)
	, F2(0.06)
	, F3(0.07)
	, P1(0)
	, P2(0)
	, P3(0)
	, deltaT(5)
	, T1(0)
	, T2(0)
	, NumberOfSatellites(3)
	, NumberOfSources(4)
	, shum(10)
	, NumberMax(1)
	//, Info(_T(""))
	, Info(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLotsOfSignalsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT3, A1);
	DDX_Text(pDX, IDC_EDIT2, A2);
	DDX_Text(pDX, IDC_EDIT1, A3);
	DDX_Text(pDX, IDC_EDIT5, F1);
	DDX_Text(pDX, IDC_EDIT4, F2);
	DDX_Text(pDX, IDC_EDIT6, F3);
	DDX_Text(pDX, IDC_EDIT8, P1);
	DDX_Text(pDX, IDC_EDIT7, P2);
	DDX_Text(pDX, IDC_EDIT9, P3);
	DDX_Text(pDX, IDC_EDIT10, deltaT);
	DDX_Control(pDX, IDC_bpsk, bpsk);
	DDX_Control(pDX, IDC_msk, msk);
	DDX_Control(pDX, IDC_Sputnic2, Sputnic2);
	DDX_Control(pDX, IDC_Sputnic3, Sputnic3);
	DDX_Control(pDX, IDC_Sputnic4, Correlator);
	DDX_Control(pDX, IDC_Sputnic5, Shema);
	DDX_Text(pDX, IDC_EDIT12, T1);
	DDX_Text(pDX, IDC_EDIT11, T2);
	DDX_Text(pDX, IDC_EDIT13, NumberOfSatellites);
	DDX_Text(pDX, IDC_EDIT14, NumberOfSources);
	DDX_Text(pDX, IDC_EDIT15, shum);
	DDX_Control(pDX, IDC_SLIDER1, spin_x);
	DDX_Control(pDX, IDC_SLIDER2, spin_y);
	DDX_Control(pDX, IDC_SLIDER3, spin_z);
	DDX_Control(pDX, IDC_SLIDER4, zoom);
	DDX_Text(pDX, IDC_NumberMax, NumberMax);
	DDX_Control(pDX, IDC_LIST1, LBox);
	//DDX_Text(pDX, IDC_MAX, Info);
	DDX_Text(pDX, IDC_EDIT16, Info);
}

BEGIN_MESSAGE_MAP(CLotsOfSignalsDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_Pusk, &CLotsOfSignalsDlg::OnBnClickedPusk)
	ON_BN_CLICKED(IDC_Pusk2, &CLotsOfSignalsDlg::OnBnClickedPusk2)
	ON_BN_CLICKED(IDC_Estimation, &CLotsOfSignalsDlg::OnBnClickedEstimation)
	ON_BN_CLICKED(IDC_Pusk3, &CLotsOfSignalsDlg::OnBnClickedPusk3)
	ON_BN_CLICKED(IDC_Pusk4, &CLotsOfSignalsDlg::OnBnClickedPusk4)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_otrisovat, &CLotsOfSignalsDlg::OnBnClickedotrisovat)
END_MESSAGE_MAP()


// Обработчики сообщений CLotsOfSignalsDlg

BOOL CLotsOfSignalsDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок
	srand(time(0));
	PicWnd_Input_Sign = GetDlgItem(IDC_AKP);
	PicDc_Input_Sign = PicWnd_Input_Sign->GetDC();
	PicWnd_Input_Sign->GetClientRect(&Pic_Input_Sign);

	PicWnd_AKP = GetDlgItem(IDC_AKP2);
	PicDc_AKP = PicWnd_AKP->GetDC();
	PicWnd_AKP->GetClientRect(&Pic_AKP);

	PicWnd_Cor1 = GetDlgItem(IDC_Cor);
	PicDc_Cor1 = PicWnd_Cor1->GetDC();
	PicWnd_Cor1->GetClientRect(&Pic_Cor1);

	PicWnd_Cor2 = GetDlgItem(IDC_Cor2);
	PicDc_Cor2 = PicWnd_Cor2->GetDC();
	PicWnd_Cor2->GetClientRect(&Pic_Cor2);

	PicWnd_MAX = GetDlgItem(IDC_MAX);
	PicDc_MAX = PicWnd_MAX->GetDC();
	PicWnd_MAX->GetClientRect(&Pic_MAX);


	setka_pen.CreatePen(		//для сетки
		PS_DOT,					//пунктирная
		0.1,						//толщина 1 пиксель
		RGB(0, 0, 250));		//цвет  черный
	osi_pen.CreatePen(			//координатные оси
		PS_SOLID,				//сплошная линия
		1,						//толщина 2 пикселя
		RGB(0, 0, 0));			//цвет черный

	graf_pen.CreatePen(			//график
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(0, 0, 255));			//цвет черный
	graf_pen2.CreatePen(PS_SOLID, 1, RGB(220, 0, 0));
	graf_pen3.CreatePen(PS_SOLID, 2, RGB(155, 150, 150));
	
	CButton* pcb1 = (CButton*)(this->GetDlgItem(IDC_bpsk));
	pcb1->SetCheck(1);

	CButton* pcb2 = (CButton*)(this->GetDlgItem(IDC_Sputnic2));
	pcb2->SetCheck(1);

	CButton* pcb3 = (CButton*)(this->GetDlgItem(IDC_Sputnic4));
	pcb3->SetCheck(1);

	activated = 0;
	

	zoom.SetRangeMin(0, TRUE);			// минимум
	zoom.SetRangeMax(1000, TRUE);
	zoom.SetPos(200);

	//spin_x.SetRangeMin(-180, TRUE);			// минимум
	//spin_x.SetRangeMax(180, TRUE);
	//spin_x.SetPos(-60);
	//spin_y.SetRangeMin(-180, TRUE);			// минимум
	//spin_y.SetRangeMax(180, TRUE);
	//spin_y.SetPos(0);
	//spin_z.SetRangeMin(-180, TRUE);			// минимум
	//spin_z.SetRangeMax(180, TRUE);
	//spin_z.SetPos(40);

	spin_x.SetRangeMin(-180, TRUE);			// минимум
	spin_x.SetRangeMax(180, TRUE);
	spin_x.SetPos(0);
	spin_y.SetRangeMin(-180, TRUE);			// минимум
	spin_y.SetRangeMax(180, TRUE);
	spin_y.SetPos(0);
	spin_z.SetRangeMin(-180, TRUE);			// минимум
	spin_z.SetRangeMax(180, TRUE);
	spin_z.SetPos(0);
	
	//GenerateTwoLongSignal();

	pDC = GetDlgItem(IDC_OPGL)->GetDC();
	InitiateOPGL();
	
	timet = 0;
	CString str;
	for (int i = 0; i < NumberOfSatellites; i++)
	{
		str.Format(_T("Корреляция №%d"), i+1);
		LBox.AddString(str);
	}
	for (int i = 0; i < NumberOfSatellites; i++)
	{
		str.Format(_T("Функция неопределенности №%d"), i + 1);
		LBox.AddString(str);
	}


	/*CListBox* pList1 = (CListBox*)GetDlgItem(IDC_LIST1);

	int nSel = pList1->GetCurSel();
	if (nSel != LB_ERR)
	{
		CString ItemSelected;
		pList1->GetText(nSel, ItemSelected);
		AfxMessageBox(ItemSelected);
	}*/
	
	
	/*string[] countries = { "Бразилия", "Аргентина", "Чили", "Уругвай", "Колумбия" };
	LBox.Items.AddRange(countries);
	LBox.Items.Insert(1, "Парагвай");
	LBox.*/
	//string firstElement = LBox.Items[0];
	
	/*DWORD identif;
	hThrds= CreateThread(NULL,0,opglstream, this,0,&identif);*/
	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CLotsOfSignalsDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		//GenerateTwoLongSignal();
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CLotsOfSignalsDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CLotsOfSignalsDlg::Draw1Graph(std::vector<double>& Mass, CDC* WinDc, CRect WinPic, CPen* graphpen, int KolToch, int AbsMax, CString Abs, CString Ord)
{
	// поиск максимального и минимального значения
	xmin = Mass[0];
	xmax = Mass[0];
	for (int i = 0; i < Mass.size(); i++)
	{
		if (Mass[i] < xmin)
		{
			xmin = Mass[i];
		}
		if (Mass[i] > xmax)
		{
			xmax = Mass[i];
		}
	}
	/*int itermax = 0;
	double mean = 0;
	for (unsigned int i = 0; i < Mass.size(); i++)
	{
		double curr = Mass[i].real() * Mass[i].real() + Mass[i].imag() * Mass[i].imag();
		if (curr > mean)
		{
			itermax = i;
			mean = curr;
		}
	}
	xmax = Mass[itermax];*/

	//---- отрисовка -------------------------------------------------
	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);
	bmp.CreateCompatibleBitmap(
		WinDc,
		WinPic.Width(),
		WinPic.Height());
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	//------ заливка фона графика белым цветом ---------------------------------
	MemDc->FillSolidRect(
		WinPic,
		RGB(
			255,
			255,
			255));
	//------ отрисовка сетки координат -----------------------------------------
	pen = MemDc->SelectObject(&setka_pen);
	// вертикальные линии сетки координат
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width(); i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, 0);
		MemDc->LineTo(i, WinPic.Height());
	}
	// горизонтальные линии сетки координат
	for (float i = (float)WinPic.Height() / 10; i < WinPic.Height(); i += (float)WinPic.Height() / 10)
	{
		MemDc->MoveTo(0, i);
		MemDc->LineTo(WinPic.Width(), i);
	}
	//------ отрисовка осей ----------------------------------------------------
	pen = MemDc->SelectObject(&koordpen);
	// отрисовка оси X
	MemDc->MoveTo(2, (float)WinPic.Height() * 9 / 10);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 + 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 - 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	// деления на оси X
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width() * 24 / 25; i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, WinPic.Height() * 9 / 10 + 2);
		MemDc->LineTo(i, WinPic.Height() * 9 / 10 - 3);
	}
	// отрисовка оси Y
	MemDc->MoveTo(WinPic.Width() * 2 / 25, WinPic.Height() - 2);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	MemDc->MoveTo(WinPic.Width() * 2 / 25 - 2, 15);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	MemDc->MoveTo(WinPic.Width() * 2 / 25 + 2, 15);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	// деления на оси Y
	for (float i = (float)WinPic.Height() / 5; i < WinPic.Height(); i += (float)WinPic.Height() / 10)
	{
		MemDc->MoveTo(WinPic.Width() * 2 / 25 - 2, i);
		MemDc->LineTo(WinPic.Width() * 2 / 25 + 3, i);
	}
	//------ подписи осей --------------------------------------------
	// установка прозрачного фона текста
	MemDc->SetBkMode(TRANSPARENT);
	// установка шрифта
	MemDc->SelectObject(&fontgraph);
	// подпись оси X
	MemDc->TextOut((float)WinPic.Width() * 24 / 25 + 4, (float)WinPic.Height() * 9 / 10 + 2, Abs);
	// подпись оси Y
	MemDc->TextOut((float)WinPic.Width() * 2 / 25 + 5, 0, Ord);
	// выбор области для рисования
	xx0 = WinPic.Width() * 2 / 25;
	xxmax = WinPic.Width() * 24 / 25;
	yy0 = WinPic.Height() / 10;
	yymax = WinPic.Height() * 9 / 10;
	// отрисовка
	pen = MemDc->SelectObject(graphpen);
	MemDc->MoveTo(xx0, yymax + (Mass[0] - xmin) / (xmax - xmin) * (yy0 - yymax));
	for (int i = 0; i < Mass.size(); i++)
	{
		xxi = xx0 + (xxmax - xx0) * i / (Mass.size() - 1);
		yyi = yymax + (Mass[i] - xmin) / (xmax - xmin) * (yy0 - yymax);
		MemDc->LineTo(xxi, yyi);
	}
	//----- вывод числовых значений -----------------------------------
	// по оси абсцисс
	//for (int i = 6; i < 25; i += 5)
	//{
	//	sprintf(znach, "%5.1f", (i - 1) * (float)AbsMax / 22);
	//	MemDc->TextOut(i * WinPic.Width() / 25 + 2, WinPic.Height() * 9 / 10 + 2, CString(znach));
	//}
	//// по оси ординат
	//sprintf(znach, "%5.2f", xmax);
	//MemDc->TextOut(0, WinPic.Height() / 20 + 1, CString(znach));
	//sprintf(znach, "%5.2f", 0.75 * xmax);
	//MemDc->TextOut(0, WinPic.Height() * 5 / 20 + 1, CString(znach));
	//sprintf(znach, "%5.2f", 0.5 * xmax);
	//MemDc->TextOut(0, WinPic.Height() * 9 / 20 + 1, CString(znach));
	//sprintf(znach, "%5.2f", 0.25 * xmax);
	//MemDc->TextOut(0, WinPic.Height() * 13 / 20 + 1, CString(znach));
	//sprintf(znach, "%5.2f", 0.0);
	//MemDc->TextOutW(0, WinPic.Height() * 9 / 10 + 2, CString(znach));
	//----- вывод на экран -------------------------------------------
	WinDc->BitBlt(0, 0, WinPic.Width(), WinPic.Height(), MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CLotsOfSignalsDlg::Draw2Graph(std::vector<double>& Mass1, CPen* graph1pen, std::vector<double>& Mass2, CPen* graph2pen, CDC* WinDc, CRect WinPic, float AbsMax, CString Abs, CString Ord)
{
	//----- поиск максимального и минимального значения -----------------------------
	Mass1Min = Mass1[0];
	Mass1Max = Mass1[0];
	Mass2Min = Mass2[0];
	Mass2Max = Mass2[0];
	for (int i = 1; i < N; i++)
	{
		if (Mass1[i] < Mass1Min)
		{
			Mass1Min = Mass1[i];
		}
		if (Mass1[i] > Mass1Max)
		{
			Mass1Max = Mass1[i];
		}
		/*if (Mass2[i] < Mass2Min)
		{
			Mass2Min = Mass2[i];
		}
		if (Mass2[i] > Mass2Max)
		{
			Mass2Max = Mass2[i];
		}*/
	}
	for (int i = 0; i < N / 4; i++)
	{
		if (Mass2[i] < Mass2Min)
		{
			Mass2Min = Mass2[i];
		}
		if (Mass2[i] > Mass2Max)
		{
			Mass2Max = Mass2[i];
		}
	}
	if (Mass2Max > Mass1Max)
	{
		Max = Mass2Max;
	}
	else
	{
		Max = Mass1Max;
	}
	if (Mass2Min < Mass1Min)
	{
		Min = Mass2Min;
	}
	else
	{
		Min = Mass1Min;
	}
	//---- отрисовка -------------------------------------------------
	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);
	bmp.CreateCompatibleBitmap(
		WinDc,
		WinPic.Width(),
		WinPic.Height());
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	//------ заливка фона графика белым цветом ---------------------------------
	MemDc->FillSolidRect(
		WinPic,
		RGB(
			255,
			255,
			255));
	//------ отрисовка сетки координат -----------------------------------------
	pen = MemDc->SelectObject(&setka_pen);
	// вертикальные линии сетки координат
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width(); i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, 0);
		MemDc->LineTo(i, WinPic.Height());
	}
	// горизонтальные линии сетки координат
	for (float i = (float)WinPic.Height() / 10; i < WinPic.Height(); i += (float)WinPic.Height() / 10)
	{
		MemDc->MoveTo(0, i);
		MemDc->LineTo(WinPic.Width(), i);
	}
	//------ отрисовка осей ----------------------------------------------------
	pen = MemDc->SelectObject(&osi_pen);
	// отрисовка оси X
	MemDc->MoveTo(2, (float)WinPic.Height() * 9 / 10);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 + 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 - 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	// деления на оси X
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width() * 24 / 25; i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, (float)WinPic.Height() * 9 / 10 + 2);
		MemDc->LineTo(i, (float)WinPic.Height() * 9 / 10 - 3);
	}
	// отрисовка оси Y
	MemDc->MoveTo(WinPic.Width() * 2 / 25, WinPic.Height() - 2);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	MemDc->MoveTo(WinPic.Width() * 2 / 25 - 2, 15);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	MemDc->MoveTo(WinPic.Width() * 2 / 25 + 2, 15);
	MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	// деления на оси Y
	for (float i = (float)WinPic.Height() / 5; i < WinPic.Height(); i += (float)WinPic.Height() / 10)
	{
		MemDc->MoveTo(WinPic.Width() * 2 / 25 - 2, i);
		MemDc->LineTo(WinPic.Width() * 2 / 25 + 3, i);
	}
	//------ подписи осей --------------------------------------------
	// установка прозрачного фона текста
	MemDc->SetBkMode(TRANSPARENT);
	// установка шрифта
	MemDc->SelectObject(&fontgraph);
	// подпись оси X
	MemDc->TextOut((float)WinPic.Width() * 24 / 25 + 4, (float)WinPic.Height() / 2 + 2, Abs);
	// подпись оси Y
	MemDc->TextOut((float)WinPic.Width() * 2 / 25 + 5, 0, Ord);
	// выбор области для рисования
	xx0 = WinPic.Width() * 2 / 25;
	xxmax = WinPic.Width() * 24 / 25;
	yy0 = WinPic.Height() / 10;
	yymax = WinPic.Height() * 9 / 10;
	// отрисовка первого графика
	pen = MemDc->SelectObject(graph1pen);
	MemDc->MoveTo(xx0, yymax + (Mass1[0] - Min) / (Max - Min) * (yy0 - yymax));
	for (int i = 0; i < N; i++)
	{
		xxi = xx0 + (xxmax - xx0) * i / (N - 1);
		yyi = yymax + (Mass1[i] - Min) / (Max - Min) * (yy0 - yymax);
		MemDc->LineTo(xxi, yyi);
	}
	// отрисовка второго графика
	pen = MemDc->SelectObject(graph2pen);
	MemDc->MoveTo(xx0, yymax + (Mass2[0] - Min) / (Max - Min) * (yy0 - yymax));
	for (int i = 0; i < N/4; i++)
	{
		xxi = xx0 + (xxmax - xx0) * i / (N - 1);
		yyi = yymax + (Mass2[i] - Min) / (Max - Min) * (yy0 - yymax);
		MemDc->LineTo(xxi, yyi);
	}
	//----- вывод на экран -------------------------------------------
	WinDc->BitBlt(0, 0, WinPic.Width(), WinPic.Height(), MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

double CLotsOfSignalsDlg::signal(double t)
{
	double Amplitude[] = { A1, A2, A3 };
	double Frequency[] = { F1, F2, F3 };
	double Phase[] = { P1, P2, P3 };
	double result = 0;
	for (int i = 0; i <= 2; i++)
	{
		result += Amplitude[i] * sin(2 * /*M_PI*/3.14159 * Frequency[i] * t + Phase[i]);
	}
	return result;
}


void CLotsOfSignalsDlg::correlate(std::vector<double>& signal1, std::vector<double>& signal2, vector<complex<double>> correlation, vector<double>& corr)
{
	for (unsigned int n = 0; n < (signal1.size() - signal2.size()); n++)
	{
		complex<double> counter = complex<double>(0, 0);
		for (unsigned int m = 0; m < signal2.size(); m++)
		{
			if ((m + n) >= 0 && (m + n) < signal1.size())
			{
				counter += signal2[m] * conj(signal1[m + n]);
			}
			else continue;
		}
		counter /= (double)signal1.size();
		correlation.push_back(counter);
		corr.push_back(correlation[n].real() * correlation[n].real() + correlation[n].imag() * correlation[n].imag());
		//correlation.push_back(n);
	}
}


void CLotsOfSignalsDlg::OnBnClickedPusk()
{
	UpdateData(true);
	std::vector<double> signal1;
	std::vector<double> signal2;
	for (double i = 0; i < N; i++)
	{
		signal1.push_back(signal(i));
		//signal2.push_back(Signal(i+deltaT));
	}
	for (double i = 0; i < N/4; i++)
	{
		signal2.push_back(signal(i+deltaT));
	}
	Draw2Graph(signal1, &graf_pen2, signal2, &graf_pen3, PicDc_Input_Sign, Pic_Input_Sign, N, CString("x"),
		CString("Ampl"));
	std::vector<complex<double>> cor;
	std::vector<double> curr;
	correlate(signal1,signal2,cor,curr);
	for (double i = 0; i < cor.size(); i++)
	{
		curr.push_back(cor[i].real() * cor[i].real() + cor[i].imag() * cor[i].imag());
	}
	Draw1Graph(curr, PicDc_AKP, Pic_AKP,&graf_pen3,N,N,CString("x"),CString("Ampl"));
}



void CLotsOfSignalsDlg::OnBnClickedPusk2()
{
	GenerateTwoLongSignal();
}

void CLotsOfSignalsDlg::find_max_min(vector<vector<double>>& D, double& max, double& min, double& jmax, double& imax)
{
	max = D[0][0];
	min = D[0][0];
	for (int j = 1; j < D.size() - 1; j++)
	{
		for (int i = 1; i < D[0].size() - 1; i++)
		{

			if (max < D[j][i])
			{
				max = D[j][i];
				jmax = j;
				imax = i;
			}
			if (min > D[j][i])
			{
				min = D[j][i];
			}
		}
	}
}

BOOL CLotsOfSignalsDlg::bSetupPixelFormat()
{
	static PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR),  // size of this pfd
		1,                              // version number
		PFD_DRAW_TO_WINDOW |            // support window
		  PFD_SUPPORT_OPENGL |          // support OpenGL
		  PFD_DOUBLEBUFFER,             // double buffered
		PFD_TYPE_RGBA,                  // RGBA type
		24,                             // 24-bit color depth
		0, 0, 0, 0, 0, 0,               // color bits ignored
		0,                              // no alpha buffer
		0,                              // shift bit ignored
		0,                              // no accumulation buffer
		0, 0, 0, 0,                     // accum bits ignored
		32,                             // 32-bit z-buffer
		0,                              // no stencil buffer
		0,                              // no auxiliary buffer
		PFD_MAIN_PLANE,                 // main layer
		0,                              // reserved
		0, 0, 0                         // layer masks ignored
	};
	int pixelformat;
	if ((pixelformat = ChoosePixelFormat(pDC->GetSafeHdc(), &pfd)) == 0)
	{
		MessageBox(L"ChoosePixelFormat failed");
		return FALSE;
	}

	if (SetPixelFormat(pDC->GetSafeHdc(), pixelformat, &pfd) == FALSE)
	{
		MessageBox(L"SetPixelFormat failed");
		return FALSE;
	}
	return TRUE;
}

void CLotsOfSignalsDlg::InitiateOPGL()
{
	CRect rect, rect1;
	HGLRC hrc;
	if (!bSetupPixelFormat())
	{
		return;
	}
	hrc = wglCreateContext(pDC->GetSafeHdc());
	ASSERT(hrc != NULL);
	wglMakeCurrent(pDC->GetSafeHdc(), hrc);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1, 1, -1, 1, -1, 1);
	gluLookAt(0, 0, -1, 0, 0, 1, 0, 100, 0);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	//gluPerspective(15.0f, (GLfloat)rect.right / (GLfloat)rect.bottom, 0.1f, 100.0f);
	glMatrixMode(GL_MODELVIEW);

}

double CLotsOfSignalsDlg::findmax(vector<vector<double>>& D, bool max_min)
{
	double find = D[0][0];

	if (max_min)
	{
		for (int i = 0; i < D.size(); i++)
		{
			for (int j = 0; j < D[i].size(); j++)
			{
				if (D[i][j] > find)
				{
					find = D[i][j];
				}
			}
		}
	}
	else
	{
		for (int i = 0; i < D.size(); i++)
		{
			for (int j = 0; j < D[i].size(); j++)
			{
				if (D[i][j] < find)
				{
					find = D[i][j];
				}
			}
		}
	}
	return find;
}

void fillrect(float x, float y, float width, float height)
{
	glBegin(GL_QUADS);
	glVertex2f(x, y);
	glVertex2f(x, y - height);
	glVertex2f(x + width, y - height);
	glVertex2f(x + width, y);
	glEnd();
}

void CLotsOfSignalsDlg::DrawGL(vector<vector<double>> WV)
{
	
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		gluLookAt(0, 0, 0, 0, 0, 1, 0, 1, 0);
		glRotated((double)spin_x.GetPos(), 1.0f, 0.0f, 0.0f);
		glRotated((double)spin_y.GetPos(), 0.0f, 1.0f, 0.0f);
		glRotated((double)spin_z.GetPos(), 0.0f, 0.0f, 1.0f);
		double mash = (double)zoom.GetPos() / 200;
		if (firsttimedraw)//1
		{
			max = findmax(WV, 1);
			min = findmax(WV, 0);
			if (abs(max) > abs(min))
			{
				absmax = abs(max);
			}
			else
			{
				absmax = abs(min);
			}
			maxmin = abs(max) + abs(min);
			firsttimedraw = 0;
		}
		glPointSize(1);

		for (int i = 1; i < WV.size() - 2; i++)
		{
			for (int j = 1; j < WV[i].size() - 1; j++)
			{
				glBegin(GL_TRIANGLE_STRIP);
				
				glColor3d((WV[i][j] -min) / maxmin*100, 0.0f, 1 - (WV[i][j] - min) / maxmin);
				glVertex3d((double)(j - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i][j] / 2 / absmax / mash);
				
				glColor3d((WV[i + 1][j] - min) / maxmin*100, 0.0f, 1 - (WV[i + 1][j] - min) / maxmin);
				glVertex3d((double)(j - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i + 1 - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i + 1][j] / 2 / absmax / mash);
				
				glColor3d((WV[i][j + 1] - min) / maxmin*100, 0.0f, 1 - (WV[i][j + 1] - min) / maxmin);
				glVertex3d((double)(j + 1 - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i][j + 1] / 2 / absmax / mash);

				glColor3d((WV[i + 1][j] - min) / maxmin*100, 0.0f, 1 - (WV[i + 1][j] - min) / maxmin);
				glVertex3d((double)(j - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i + 1 - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i + 1][j] / 2 / absmax / mash);
				
				glColor3d((WV[i][j + 1] - min) / maxmin*100, 0.0f, 1 - (WV[i][j + 1] - min) / maxmin);
				glVertex3d((double)(j + 1 - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i][j + 1] / 2 / absmax / mash);
				
				glColor3d((WV[i + 1][j + 1] - min) / maxmin*100, 0.0f, 1 - (WV[i + 1][j + 1] - min) / maxmin);
				glVertex3d((double)(j + 1 - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i + 1 - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i + 1][j + 1] / 2 / absmax / mash);

				glEnd();
			}
		}

					
		
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		if (lines)
		{
			glLineWidth(3);
			glBegin(GL_QUADS);
			for (int i = steplines; i < WV.size() - steplines; i += steplines)
			{
				for (int j = steplines; j < WV[i].size() - steplines; j += steplines)
				{
					for (int k = 0; k < del.size(); k++)
					{
						/*glColor3d(1.0f, 1.0f, 1.0f);
						if (abs(i - del[k][1]) < 10 && abs(j - del[k][0]) < 10)
						{*/
							glColor3d(1.0f, 1.0f, 1.0f);

							glVertex3d((double)(j - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i][j] / 2 / absmax / mash + 1. / 50 / mash);
							glVertex3d((double)(j - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i + steplines - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i + steplines][j] / 2 / absmax / mash + 1. / 50 / mash);
							glVertex3d((double)(j + steplines - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i + steplines - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i + steplines][j + steplines] / 2 / absmax / mash + 1. / 50 / mash);
							glVertex3d((double)(j + steplines - (double)dimx / 2) * 2 / (double)dimx / mash, (double)(i - (double)dimy / 2) * 2 / (double)dimy / mash, WV[i][j + steplines] / 2 / absmax / mash + 1. / 50 / mash);
						//}	
						
					}
				}
			}
			glEnd();

		}
		glFinish();
		SwapBuffers(wglGetCurrentDC());
	
}

void CLotsOfSignalsDlg::PererisovkaGetData(CDC* WinDc, CRect WinPic, CPen* graphpen, double MinX, double MaxX, double MinY, double MaxY)
{

	xminget = MinX;			//минимальное значение х
	xmaxget = MaxX;			//максимальное значение х
	yminget = MinY;			//минимальное значение y
	ymaxget = MaxY;			//максимальное значение y


	xpget = ((double)(WinPic.Width()) / (xmaxget - xminget));			//Коэффициенты пересчёта координат по Х
	ypget = -((double)(WinPic.Height()) / (ymaxget - yminget));

	WinDc->FillSolidRect(&WinPic, RGB(255, 255, 255));			//закрашиваю фон 


	WinDc->SelectObject(&osi_pen);
	//создаём Ось Y
	WinDc->MoveTo(KOORDGET(0, ymaxget));
	WinDc->LineTo(KOORDGET(0, yminget));
	//создаём Ось Х
	WinDc->MoveTo(KOORDGET(xminget, 0));
	WinDc->LineTo(KOORDGET(xmaxget, 0));

	//подпись осей
	WinDc->TextOutW(KOORDGET(0, ymaxget - 0.2), _T("")); //Y
	WinDc->TextOutW(KOORDGET(xmaxget - 0.3, 0 - 0.2), _T("A")); //X

	WinDc->SelectObject(&setka_pen);
	double shagX = (xmaxget - xminget) / 10;
	//отрисовка сетки по y
	for (float x = xminget; x <= xmaxget; x += shagX)
	{
		WinDc->MoveTo(KOORDGET(x, ymaxget));
		WinDc->LineTo(KOORDGET(x, yminget));
	}
	double shagY = (ymaxget - yminget) / 10;
	//отрисовка сетки по x
	for (float y = yminget + 1; y <= ymaxget; y += shagY)
	{
		WinDc->MoveTo(KOORDGET(xminget, y));
		WinDc->LineTo(KOORDGET(xmaxget, y));
	}


	//подпись точек на оси
	CFont font3;
	font3.CreateFontW(15, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	WinDc->SelectObject(font3);
	//по Y с шагом 5
	for (double i = yminget; i <= ymaxget; i += shagY)
	{
		CString str;
		str.Format(_T("%.1f"), i);
		WinDc->TextOutW(KOORDGET(0.01, i), str);
	}
	//по X с шагом 0.5
	for (double j = xminget; j <= xmaxget; j += shagX)
	{
		CString str;
		str.Format(_T("%.1f"), j);
		WinDc->TextOutW(KOORDGET(j, -0.05), str);
	}
}


double func(int x, int y, vector<vector<double>> z)
{
	return z[x][y];
}

void CLotsOfSignalsDlg::GenerateTwoLongSignal()
{
	UpdateData(true);
	std::vector<double> SSignal;
	std::vector<double> SSignal2;
	std::vector<double> SSignal3;
	sIgnal2.clear();
	sIgnal3.clear();
	SSignal.clear();

	SignalType signal_type;
	if (bpsk.GetCheck())
		signal_type = BPSK;
	if (msk.GetCheck())
		signal_type = MSK;

	//vector<Signal> massLongSignal(NumberOfSources);
	vector<Signal>* massLongSignal = new vector<Signal>(NumberOfSources);

	//samplingFrequency = 300e5;
	Signal LongSignal1, LongSignal2;
	
	SignalGenerationParameters parametrs = {startTimestamp,startPhase,Duration,nSamples,
		samplingFrequency,Bitrate,additionalParameter };
	for (int i = 0; i < NumberOfSources; i++)
	{
		SignalGenerator ss;
		ss.GenerateSignal(signal_type, parametrs, (*massLongSignal)[i]);

		parametrs = {startTimestamp,startPhase,Duration,nSamples,
			samplingFrequency,Bitrate,additionalParameter};
	}

	//vector<vector<Signal>> massSignal(NumberOfSources, vector<Signal>(NumberOfSatellites));
	//vector<vector<Signal>> massSignal(NumberOfSatellites, vector<Signal>(NumberOfSources));
	vector<vector<Signal>>* massSignal = new vector<vector<Signal>>(NumberOfSatellites, vector<Signal>(NumberOfSources));

	//vector<Signal> SummSignal(NumberOfSatellites);
	vector<Signal>* SummSignal = new vector<Signal>(NumberOfSatellites);

	vector<double> sdvigtime;
	sdvigtime.clear();
	sdvigtime = Delays(NumberOfSources, NumberOfSatellites);

	vector<double> sdvigFrequency;
	sdvigFrequency.clear();
	sdvigFrequency = DelaysFrequency(NumberOfSources, NumberOfSatellites);

	//случайные доплеры с разницей не больше fd/2
	int k = 0;
	double d=Duration/150;
	
	for (int i = 0; i < NumberOfSatellites; i++)
	{
		for (int j = 0; j < NumberOfSources; j++)
		{
			Methods::transformSignal((*massLongSignal)[j], sdvigtime[i+ NumberOfSatellites*j] * d, d, sdvigFrequency[i + NumberOfSatellites * j]*d, 1, shum, (*massSignal)[i][j]);
		}
		for (int j = 1; j < NumberOfSources; j++)
		{
			if(j==1)	Methods::coherentSumm({(*massSignal)[i][0] , (*massSignal)[i][j] }, (*SummSignal)[i]);
			else		Methods::coherentSumm({ (*SummSignal)[i] , (*massSignal)[i][j] }, (*SummSignal)[i]);			
		}
	}
	delete massLongSignal;
	delete massSignal;
	
	
	for (int i = 0; i < NumberOfSatellites; i++)
	{
		if (i + 1 == NumberOfSatellites) Methods::Newcorrelate((*SummSignal)[i], (*SummSignal)[0], (*CorrelateSignal)[i]);
		else Methods::Newcorrelate((*SummSignal)[i], (*SummSignal)[i + 1], (*CorrelateSignal)[i]);
	}
	
	////////////////////////
	/*vector<Signal2D>* outsignal = new vector<Signal2D>(NumberOfSatellites);*/
	//Signal2D outsignal;
	for (int i = 0; i < NumberOfSatellites; i++)
	{
		if (i + 1 == NumberOfSatellites) Methods::NewAmbiguityFunction((*SummSignal)[i], (*SummSignal)[0], (*outsignal)[i],scale);
		else Methods::NewAmbiguityFunction((*SummSignal)[i], (*SummSignal)[i + 1], (*outsignal)[i],scale);
	}
		//Methods::NewAmbiguityFunction((*SummSignal)[0], (*SummSignal)[1], outsignal);
		delete SummSignal;
		//Signal2D cutoutsignal;
		clearance = 1.0 / Bitrate * samplingFrequency;
		fr = 2 * scale;
		for (int i = 0; i < NumberOfSatellites; i++)
		{ 
			cutFrequency((*outsignal)[i]);
			sput = i;
			del=find_max_TF(NumberOfSources/* * NumberOfSatellites*/, (*outsignal)[i], clearance);
		}
		
		/*outsignal2D.resize(outsignal.signal.size());
		for (int i = 0; i < outsignal.signal.size(); i++)
		{
			outsignal2D[i].resize(outsignal.signal[i].size());
			for (int j = 0; j < outsignal.signal[i].size(); j++)
			{
				outsignal2D[i][j] = sqrt(outsignal.signal[i][j].real() * outsignal.signal[i][j].real() + outsignal.signal[i][j].imag() * outsignal.signal[i][j].imag());
			}
		}*/
	////////////////////////////
	//	/*double max = 0, min = 0, jmax = 10, imax = 10;
	//	find_max_min(outsignal2D, max, min, jmax, imax);
	//	for (int i = 0; i < outsignal2D.size(); i++)
	//	{
	//		for (int j = 0; j < outsignal2D[i].size(); j++)
	//		{
	//			if (outsignal2D[i][j] == max)
	//			{
	//				jmax = j;
	//				break;
	//			}
	//		}
	//	}*/
	//	
	//	//ofstream fout("Нечто непределенное.txt");
	//	//fout << "размеры матрицы" << outsignal2D.size()<< "\t" <<outsignal2D[0].size()<<"\n";
	//	//for (int i = 0; i < outsignal2D.size(); i++)
	//	//{
	//	//	for (int j = 0; j < outsignal2D[i].size(); j++)
	//	//	{
	//	//		fout << setprecision(3)<<outsignal2D[i][j] << "\t";
	//	//	}
	//	//	fout <<  "\n";
	//	//}
	//	//fout.close(); // закрываем файл
	//


	//	/*firsttimedraw=1;
	//	DrawGL(outsignal2D);*/

	///////////////////////////////////////////////////////////////////////////////////////
	//	//vector<double> corr;
	//	///*corr.resize(outsignal.signal.size());
	//	//for (int i = 0; i < corr.size()-1; i++)
	//	//{
	//	//	corr[i] = abs(outsignal.signal[i][del[NumberMax][1]]);
	//	//}*/
	//	//corr.resize((*CorrelateSignal)[1].signal.size());
	//	//for (int i = 0; i < corr.size() - 1; i++)
	//	//{
	//	//	corr[i] = abs((*CorrelateSignal)[1].signal[i]);
	//	//}
	//	//double Maxx=0;
	//	//for (int i = 0; i < corr.size() - 1; i++)
	//	//{
	//	//	if (Maxx < corr[i]) Maxx = corr[i];
	//	//}
	//	//
	//	//PererisovkaGetData(PicDc_MAX, Pic_MAX, &graf_pen,
	//	//	-0.01,
	//	//	Maxx,
	//	//	(*CorrelateSignal)[1].keys[0],
	//	//	(*CorrelateSignal)[1].keys[(*CorrelateSignal)[0].keys.size() - 1]
	//	//	);
	//	//PicDc_MAX->SelectObject(&graf_pen2);
	//	//PicDc_MAX->MoveTo(KOORDGET(corr[0], (*CorrelateSignal)[1].keys[0]));
	//	//for (double i = 0; i < corr.size(); i ++)
	//	//{
	//	//	PicDc_MAX->LineTo(KOORDGET(corr[i], (*CorrelateSignal)[1].keys[i]));
	//	//}
		string res = "";
		res = res + "\r\n" + "Результат работы критерия по корреляционным функциям:\r\n";
		Info = res.c_str();
		vector<vector<int>> delays;
		delays.clear();
		delays.resize(NumberOfSatellites);
		for (int i = 0; i < NumberOfSatellites; i++)
		{
			auto delays1 = find_max_n(NumberOfSources, (*CorrelateSignal)[i], clearance);
			delays[i] = delays1;
		}
		ind.resize(NumberOfSources);
		vector<double> SumDelay;
		SumDelay.clear();
		SumDelay = criteria(delays);
		KolOtch = SumDelay.size();
		ofstream out("Куча времени2.txt");
		out << "Номера задержек" << "\t" << "Сумма задержек" << "\n";
		for (int i = 0; i < SumDelay.size(); i++)
		{
			out << str[i] << "\t" << SumDelay[i] << "\n";
			res = res + str[i] + "\t\t" + to_string(SumDelay[i]) + "\r\n";
		}
		out.close(); // закрываем файл
		Info += res.c_str();
		
		
		res = res + "\r\n" + "Результат работы критерия по функциям неопределенности:\r\n";

		vector<double> SummDelays;
		SummDelays.clear();
		SummDelays = criteria(Time, clearance);
		KolOtch = SummDelays.size();

		ofstream tout("Куча времени.txt");
		tout << "Номера задержек" << "\t" << "Сумма задержек" << "\n";
		for (int i = 0; i < SummDelays.size(); i++)
		{
			tout << str1[i] << "\t\t" << SummDelays[i] << "\n";
			tout << str3[i] << "\n\n";
			
			res = res + str1[i] + "\t\t" + to_string(SummDelays[i]) + "\r\n";
		}
		
		res = res + "\r\n" + "Группы задержек и их сумма:\r\n";
		
		for (int i = 0; i < Stroka1.size(); i++)
		{
			
			tout << Stroka1[i] << "\t\t" << Stroka2[i] << "\n";
			res = res + Stroka1[i] + "                         "+ Stroka2[i]+"\r\n";
		}
		tout.close(); // закрываем файл
		Info = res.c_str();
		str1.clear();
		vector<double> SummFreq;
		SummFreq.clear();
		//scale = (*outsignal)[0].sampling / (*mass).size();
		SummFreq = criteria(Freq, 2 * scale);

		ofstream fout("Куча частот.txt");
		fout << "Номера задержек" << "\t" << "Сумма частот" << "\n";
		for (int i = 0; i < SummFreq.size(); i++)
		{
			fout << str1[i] << "\t\t" << SummFreq[i] << "\n";
			fout << str2[i] << "\n\n";
		}
		fout.close(); // закрываем файл

		

		UpdateData(false);
		
		
}

void CLotsOfSignalsDlg::Research()
{
	UpdateData(true);
	Shum = -10;
	double Kol = 0;
	double KolPrav = 0;
	double KolOne = 0;
	double KolTwo = 0;
	vector<double>verout;
	vector<double>veroutone;
	vector<double>verouttwo;
	verout.clear();
	veroutone.clear();
	verouttwo.clear();
	for (double i = NumberOfSources; i < NumberOfSources+5; i++)
	{
		KolMax = i;
		KolPrav = 0;
		Kol = 0;
		KolOne = 0;
		KolTwo = 0;
		for (int j = 0; j < 5; j++)
		{
			GenerateTwoLongSignal();
			if (KolOtch == NumberOfSources) KolPrav++;
			if (KolOtch == NumberOfSources - 1 || KolOtch == NumberOfSources) KolOne++;
			if (KolOtch == NumberOfSources - 2 || KolOtch == NumberOfSources - 1 || KolOtch == NumberOfSources) KolTwo++;

			Kol++;
		}
		verout.push_back(KolPrav / Kol);
		veroutone.push_back(KolOne/Kol);
		verouttwo.push_back(KolTwo / Kol);
	}
	ofstream newis("Ещё одно исследование.txt");
	for (int i = 0; i < verout.size(); i++)
	{
		newis << verout[i]  << "\t" << veroutone[i] << "\t" << verouttwo[i] << "\n";
	}
	newis.close(); // закрываем файл
}



void CLotsOfSignalsDlg::ProbabilitiesResearch()
{
	UpdateData(true);
	double KolPrav = 0;
	double KolOne = 0;
	double KolTwo = 0;
	double Kol = 0;
	vector<double> Polverout;
	vector<double> veroutminusone;
	vector<double> veroutminustwo;
	for(double i = -16; i < 1; i++)
	{ 
		KolOtch = 0;
		KolPrav = 0;
		KolOne = 0;
		KolTwo = 0;
		Kol = 0;
		Shum = i;
		for (int j = 0; j < 5; j++)
		{
			GenerateTwoLongSignal();
			if (KolOtch == NumberOfSources) KolPrav++;
			if (KolOtch == NumberOfSources-1 || KolOtch == NumberOfSources) KolOne++;
			if (KolOtch == NumberOfSources - 2|| KolOtch == NumberOfSources-1 || KolOtch == NumberOfSources) KolTwo++;
			Kol++;
		}
		Polverout.push_back(KolPrav/Kol);
		veroutminusone.push_back(KolOne / Kol);
		veroutminustwo.push_back(KolTwo / Kol);
	}
	
	ofstream is("Исследование.txt");
	for (int i = 0; i < Polverout.size(); i++)
	{
		is << Polverout[i] << "\t" << veroutminusone[i]<< "\t" << veroutminustwo[i]<< "\n";
	}
	is.close(); // закрываем файл

}

void CLotsOfSignalsDlg::krug(double x, double y, double r, CDC* WinDc)
{
	WinDc->Ellipse(KOORDGET(x - r, y + 400 * r), KOORDGET(x + r, y - 400 * r));
}
void CLotsOfSignalsDlg::OnBnClickedEstimation()
{
	double t2i = T1;
	double por =1/Bitrate;
	std::vector<double> Criterion;
	for(double k=0; k<1; k+=0.2)
	{ 
		//Сдвиг второго сигнала на величину t2i
		std::vector<double> ShiftedSecondSignal;
		int iter = 0;
		for (int i = 0; i < sIgnal2.size() + t2i; i++)
		{
			if (i < t2i)ShiftedSecondSignal.push_back(0);
			else
			{
				ShiftedSecondSignal.push_back(sIgnal2[iter]);
				iter++;
			}
		}

		double LenghtS1 = SSignal.size();
		double LenghtShiftedSecondSignal = ShiftedSecondSignal.size();
		FILE* results1;
		if (fopen_s(&results1, "Help.txt", "w") == 0)
		{
			fprintf(results1, "%+*.1f ", 10, LenghtS1);
			fprintf(results1, "\n");
			fprintf(results1, "%+*.1f ", 10, LenghtShiftedSecondSignal);
			fprintf(results1, "\n");
		}
		//Суммируем сдвинутый сигал и базовый
		std::vector<double> SumOf1SignalAndShifted;
		for (int i = 0; i < LenghtShiftedSecondSignal; i++)
		{
			if (i >= LenghtS1)SumOf1SignalAndShifted.push_back(ShiftedSecondSignal[i]);
			else SumOf1SignalAndShifted.push_back(ShiftedSecondSignal[i] - SSignal[i]);
		}

		//Коррелируем третий сигнал с только что пролученной суммой
		std::vector<complex<double>> cor;
		std::vector<double> correlationOfTheThirdSignalWithTheSum;
		correlate(SumOf1SignalAndShifted, sIgnal3, cor, correlationOfTheThirdSignalWithTheSum);
		for (double i = 0; i < cor.size(); i++)
		{
			correlationOfTheThirdSignalWithTheSum.push_back(cor[i].real() * cor[i].real() + cor[i].imag() * cor[i].imag());
		}
		//Суммируем третий сигнал с только что полученной корреляцией
		double LenghtCor = correlationOfTheThirdSignalWithTheSum.size();
		double LenghtS3 = sIgnal3.size();
		FILE* results2;
		if (fopen_s(&results2, "Help2.txt", "w") == 0)
		{
			fprintf(results2, "%+*.1f ", 10, LenghtCor);
			fprintf(results2, "\n");
			fprintf(results2, "%+*.1f ", 10, LenghtS3);
			fprintf(results2, "\n");
		}

		std::vector<double> Result;
		for (int i = 0; i < LenghtS3; i++)
		{
			if (i >= LenghtCor)Result.push_back(sIgnal3[i]);
			else Result.push_back(correlationOfTheThirdSignalWithTheSum[i] - sIgnal3[i]);
		}

		//Находим второй отсчет
		double NewOtshetSignala = 0, t3i = 0;
		for (int i = 0; i < Result.size(); i++)
		{
			if (NewOtshetSignala < Result[i])
			{
				t3i = i;
				NewOtshetSignala = Result[i];
			}
		}
		t2i += por;
		//тут что-то не так(разные длины векторов, t3i лежит за пределами второго вектора)
		Criterion.push_back(sIgnal3[t3i]/ correlationOfTheThirdSignalWithTheSum[t3i]);
	}
	Draw1Graph(Criterion, PicDc_AKP, Pic_AKP, &graf_pen3, N, Criterion.size(), CString("x"), CString("Ampl"));

}



void CLotsOfSignalsDlg::OnBnClickedPusk3()
{
	//ProbabilitiesResearch();
	Research();
}


void CLotsOfSignalsDlg::OnBnClickedPusk4()
{
	/*WaitForSingleObject(waitfordraw, INFINITE);
	WaitForSingleObject(waitforgennext, INFINITE);
	ResetEvent(waitforgenstart);
	WaitForSingleObject(waitfordraw, 10);
	WaitForSingleObject(waitforgennext, 10);
	UpdateData(true);
	DrawGL(outsignal2D);
	SetEvent(waitforgenstart);*/
	hThread = CreateThread(NULL, 0, opglstream, this, 0, &dwThread);
	timer = SetTimer(1, 1, NULL);
	UpdateData(false);

	//switch (activated)
	//{
	//case 0:
	//	KillTimer(1);
	//	//GetDlgItem(IDC_Animation2)->SetWindowTextW(L"Запустить анимацию");
	//	SuspendThread(hThread);
	//	activated = 1;
	//	break;
	//case 1:
	//	SetTimer(1, 1, NULL);
	//	//GetDlgItem(IDC_Animation2)->SetWindowTextW(L"Остановить анимацию");
	//	ResumeThread(hThread);
	//	activated = 0;
	//	break;
	//}
}


void CLotsOfSignalsDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: добавьте свой код обработчика сообщений или вызов стандартного
	Invalidate(0);
	if (killtimer)
	{
		KillTimer(timer);
		UpdateData(false);
	}
	CDialogEx::OnTimer(nIDEvent);
}


void CLotsOfSignalsDlg::OnBnClickedotrisovat()
{
	int nSel = LBox.GetCurSel();
	
	for(int l=0;l<NumberOfSatellites;l++)
	{
		if(l==nSel)
		{
			vector<double> corr;
			corr.resize((*CorrelateSignal)[l].signal.size());
			for (int i = 0; i < corr.size() - 1; i++)
			{
				corr[i] = abs((*CorrelateSignal)[l].signal[i]);
			}
			double MaxY = 0;
			for (int i = 0; i < corr.size() - 1; i++)
			{
				if (MaxY < corr[i]) MaxY = corr[i];
			}
			PererisovkaGetData(PicDc_Input_Sign, Pic_Input_Sign, &graf_pen,
				(*CorrelateSignal)[l].keys[0],
				(*CorrelateSignal)[l].keys[(*CorrelateSignal)[l].keys.size() - 1],
				-0.1,
				MaxY
				);

			CFont font3;
			font3.CreateFontW(35, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
			PicDc_Input_Sign->SelectObject(font3);
			for(double i=0;i<Time[l].size();i++)
			{ 
				CString str;
				str.Format(_T("%.0f"), i);
				int it = 0;
				for (int j = 0; j < corr.size(); j++)
				{
					if ((*CorrelateSignal)[l].keys[j] == Time[l][i])  it = j;
				}
				PicDc_Input_Sign->TextOutW(KOORDGET(Time[l][i],corr[it]), str);
			}
			PicDc_Input_Sign->SelectObject(&graf_pen2);
			PicDc_Input_Sign->MoveTo(KOORDGET((*CorrelateSignal)[l].keys[0], corr[0]));
			for (double i = 0; i < corr.size(); i++)
			{
				PicDc_Input_Sign->LineTo(KOORDGET((*CorrelateSignal)[l].keys[i], corr[i]));
			}

		}
	}
	if (nSel>= NumberOfSatellites)
	{
		outsignal2D.resize((*outsignal)[nSel].signal.size());
		for (int i = 0; i < (*outsignal)[nSel].signal.size(); i++)
		{
			outsignal2D[i].resize((*outsignal)[nSel].signal[i].size());
			for (int j = 0; j < (*outsignal)[nSel].signal[i].size(); j++)
			{
				outsignal2D[i][j] = sqrt((*outsignal)[nSel].signal[i][j].real() * (*outsignal)[nSel].signal[i][j].real() 
					+ (*outsignal)[nSel].signal[i][j].imag() * (*outsignal)[nSel].signal[i][j].imag());
			}
		}
		firsttimedraw=1;
			DrawGL(outsignal2D);
	}
}
